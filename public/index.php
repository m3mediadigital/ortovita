<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="description" content="Ortovita - Qualidade em movimento">
    <meta property="og:url" content="">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Ortovita - Qualidade em movimento">
	<meta property="og:image" content="/images/background.svg">
	<meta property="og:description" content="Ortovita - Qualidade em movimento">
	<meta property="og:site_name" content="Ortovita - Qualidade em movimento">
    <meta property="og:locale" content="pt_BR">
    <meta itemprop="name" content="Ortovita - Qualidade em movimento">
	<meta itemprop="description" content="Ortovita - Qualidade em movimento">
	<meta itemprop="image" content="/images/background.svg">

	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="">
	<!-- <meta name="twitter:creator"content="@individual_account"> -->
	<meta name="twitter:url" content="">
	<meta name="twitter:title" content="Ortovita - Qualidade em movimento">
	<meta name="twitter:description" content="Ortovita - Qualidade em movimento">
	<meta name="twitter:image" content="/images/background.svg">

    <title>Ortovita - Qualidade em movimento</title>
    <!-- <base href="http://127.0.0.1:3000"> -->

    
	<link rel="author" href="NovaM3">

	
	<link rel="me" href="http://www.novam3.com/" type="text/html">
	<link rel="me" href="mailto:web@novam3.com">
    <link rel="me" href="sms:+558432072700">
    


	<link rel="icon" type="image/png" href="/images/favicon.png">
	<meta name="msapplication-TileColor" content="rgba(51, 51, 51, 0.77)">
	<meta name="msapplication-TileImage" content="/images/favicon.png">
	<meta name="theme-color" content="rgba(51, 51, 51, 0.77)">

    
    
    <link rel="stylesheet" href="css/app.css" />
</head>

<body>
    <header class="d-flex justify-content-center">
        <img src="/images/logo.svg" class="img-fluid" alt="Ortovita em Breve">
    </header>
    <section class="d-flex align-items-center">
        <main class="container">
            <div class="col-12 text-center">
                <h1>AGUARDE!</h1>
                <P><i>Em breve, o site da melhor clínica ortopédica de Natal!</i></P>
            </div>
            <div class="col-12 d-flex justify-content-center pt-5">
                <a href="https://api.whatsapp.com/send?phone=5584994941411&text=oi!" class="btn d-flex align-items-center justify-content-center">
                    <img src="/images/whatsapp.svg" alt="Whatsapp Ortovita">
                    MARQUE SUA CONSULTA
                </a>
            </div>
        </main>
    </section>
    <footer>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <nav>
                        <ul class="navbar-nav mr-auto flex-row">
                            <li class="nav-item d-flex align-items-center">
                                <p class="nav-link">Acompanhe nas redes sociais:</p>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a class="nav-link" target="_blank" href="https://www.facebook.com/ortovitanatal/">
                                    <img src="/images/facebook.svg" alt="">
                                </a>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a class="nav-link" target="_blank" href="https://www.instagram.com/ortovitanatal/">
                                    <img src="/images/instagram.svg" alt="">
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-12 col-md-6 d-flex justify-content-end">
                    <img src="/images/hospital-rn.svg" class="img-fluid" alt="Hostipal do RN">
                </div>
            </div>
        </div>
    </footer>

    <img src="/images/img-header.svg" class="header">
    <img src="/images/img-footer.svg" class="footer">

    <script src="js/app.js"></script>
</body>

</html>